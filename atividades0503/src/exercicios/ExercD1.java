package exercicios;

public class ExercD1 {
    public static void main(String[] args) {
        int a = 0;
        int b = 10;

        while (b > a) {
            b--;
            System.out.println("O Valor de B dentro do WHILE é: " + b);
        }
    }
}
