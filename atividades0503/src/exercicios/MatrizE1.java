package exercicios;

import java.util.Scanner;

public class MatrizE1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Informe o tamanho do vetor: ");
        int tamanho = sc.nextInt();
        int vetor[] = new int[tamanho];
        System.out.println("Informe os valores do vetor");

        for (int i = 0; i < tamanho; i++) {
            vetor[i] = sc.nextInt();
        }

        System.out.println("Elementos na ordem inversa");
        for (int j = tamanho - 1; j >= 0; j--) {
            System.out.print(vetor[j] + " ");
        }
        System.out.println();
        sc.close();
    }
}
