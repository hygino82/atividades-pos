package exercicios;

public class ExercA3 {
    public static void main(String[] args) {
        var nome = "Adroaldo Ferreira";
        saudacao(nome);
    }

    public static void saudacao(String nome) {
        System.out.println("Meu nome é " + nome);
    }
}
