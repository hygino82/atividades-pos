package exercicios;

import java.util.Scanner;

public class ExercF1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Informe o primeiro número: ");
		int a = sc.nextInt();
		System.out.print("Informe o segundo número: ");
		int b = sc.nextInt();

		if (a > b) {
			System.out.println(a + " é maior que " + b);
		} else if (b > a) {
			System.out.println(a + " é menor que " + b);
		} else {
			System.out.println(a + " é igual " + b);
		}
		sc.close();
	}
}
