package exercicios;

import java.util.Scanner;

public class MatrizE2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Informe o numero de linhas da matriz: ");
        int linhas = sc.nextInt();
        System.out.print("Informe o numero de colunas da matriz: ");
        int colunas = sc.nextInt();
        int matriz[][] = new int[linhas][colunas];

        System.out.println("Informe os elmentos da matriz");
        for (int i = 0; i < linhas; i++) {
            for (int j = 0; j < colunas; j++) {
                matriz[i][j] = sc.nextInt();
            }
        }

        imprimeOrdemInversaMatriz(matriz);
        sc.close();
    }

    public static void imprimeOrdemInversaMatriz(int matriz[][]) {
        for (int i = matriz.length - 1; i >= 0; i--) {
            for (int j = matriz[i].length - 1; j >= 0; j--) {
                System.out.print(matriz[i][j] + "  ");
            }
            System.out.println();
        }
    }
}
