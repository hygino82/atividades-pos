package exercicios;

import java.util.Scanner;

public class StringE1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Informe uma frase: ");
        String frase = sc.nextLine().toLowerCase();// deixas as letras minusculas para procurar uma determinada letra
        System.out.print("Informe uma letra para buscar: ");
        char letra = sc.next().toLowerCase().charAt(0);
        int posicao = frase.indexOf(letra);

        if (posicao == -1) {
            System.out.println("Não existe a letra" + letra + " na frase informada!");
        } else {
            int contador = 0;

            for (int i = 0; i < frase.length(); i++) {
                if (frase.charAt(i) == letra) {
                    contador++;
                }
            }
            System.out.println("Existe(m) " + contador + " letra(s) " + letra + " na frase informada!");
        }

        sc.close();
    }
}
