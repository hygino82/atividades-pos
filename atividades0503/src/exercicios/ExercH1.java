package exercicios;

import java.util.Scanner;

public class ExercH1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Infome um número inteiro: ");
        int numero = sc.nextInt();
        System.out.println("O número " + numero + " é " + (numero % 2 == 0 ? "Par" : "Impar"));
        sc.close();
    }
}
