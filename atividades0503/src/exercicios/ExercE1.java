package exercicios;

public class ExercE1 {
    public static void main(String[] args) {
        int a = 0;
        int b = 10;

        do {
            System.out.println("O Valor de B dentro do FOR é: " + b);
            b--;
        } while (b >= a);
    }
}
