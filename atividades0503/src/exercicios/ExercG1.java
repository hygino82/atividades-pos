package exercicios;

import java.util.Scanner;

public class ExercG1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Infome o valor de a: ");
        int a = sc.nextInt();
        System.out.print("Infome o valor de b: ");
        int b = sc.nextInt();
        System.out.println("Menu de opções");
        System.out.println("1 - Somar (a+b)");
        System.out.println("2 - Multiplicar (a*b)");
        System.out.println("3 - Subtrair (a-b)");
        System.out.println("5 - Dividir (a/b)");
        System.out.print("Escolha: ");
        int opcao = sc.nextInt();

        switch (opcao) {
            case 1:
                int soma = a + b;
                System.out.println("A soma de a+b é: " + soma);
                break;
            case 2:
                int produto = a * b;
                System.out.println("O produto de a*b é: " + produto);
                break;

            case 3:
                int diferenca = a - b;
                System.out.println("A diferença de a-b é: " + diferenca);
                break;

            case 5:
                int quociente = a / b;
                System.out.println("O quociente a/b é: " + quociente);
                break;
            default:
                System.out.println("Opção inválida!");
                break;
        }

        sc.close();
    }
}
