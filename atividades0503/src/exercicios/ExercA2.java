package exercicios;

public class ExercA2 {
    public static void main(String[] args) {
        var nome = "Adroaldo Ferreira";
        var email = "hygino@msn.com";
        var cidade = "Coronel Vivida";

        imprimeDados(nome, email, cidade);
    }

    private static void imprimeDados(String nome, String email, String cidade) {
        System.out.println("Nome: " + nome);
        System.out.println("Email: " + email);
        System.out.println("Cidade: " + cidade);
    }
}