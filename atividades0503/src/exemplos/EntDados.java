package exemplos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EntDados {
    public static void main(String arg[]) {
        System.out.println("Entre com um valor: ");
        InputStreamReader c = new InputStreamReader(System.in);
        BufferedReader cd = new BufferedReader(c);
        String s = "";
        try {
            s = cd.readLine();
            int w = Integer.parseInt(s);
            System.out.println("O valor de entradada foi: " + w);
        } catch (IOException e) {
            System.out.println("Erro de entrada");
        } catch (NumberFormatException e) {
            System.out.println("Não é um número válido!");
        }
    }
}