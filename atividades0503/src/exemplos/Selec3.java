package exemplos;

public class Selec3 {

    public static void main(String arg[]) {
        int valor = (int) (Math.random() * 5); // casting
        switch (valor) {
            case 0:
                System.out.println("Primeira Opção (Valor igual a zero) " + valor);
                break;
            case 1:
                System.out.println("Segunda Opção (Valor igual a um) " + valor);
                break;
            default:
                System.out.println("Outras Opções (Valor maior que um) " + valor);
                break;
        }
    }
}
