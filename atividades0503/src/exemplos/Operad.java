package exemplos;

public class Operad {

    public static void main(String[] args) {
        System.out.println("Impressão de resultado de cálculos:");
        int a = 3, b = 2;
        int c = a + b;
        int d = a * b;
        int e = a / b;
        int f = a - b;
        System.out.println("A = " + a + ", B = " + b);
        System.out.println("O resultado da soma de A e B é: " + c);
        System.out.println("O resultado da produto de A e B é: " + d);
        System.out.println("O resultado da divisão de A e B é: " + e);
        System.out.println("O resultado da diferença de A e B é: " + f);

    }
}
