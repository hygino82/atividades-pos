package exemplos;

public class Selec1 {

    public static void main(String[] args) {
        int a = 0, b = 20;

        for (a = 0; a <= b; a++) {
            if (a == b / 2) {
                System.out.println("O valor de a é igual a metade de B, logo A vale: " + a + " e B vale: " + b);
            } else if (a != b / 2) {
                System.out.println("O valor de a é diferente a metade de B, logo A vale: " + a + " e B vale: " + b);
            }
        }
    }
}
