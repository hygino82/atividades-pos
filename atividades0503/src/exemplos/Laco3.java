package exemplos;

public class Laco3 {
    public static void main(String[] args) {
        int a = 0, b = 15;
        do {
            System.out.println("O valor de A dentro do DO é: " + a);
            a++;
        } while (a <= b);
    }
}
