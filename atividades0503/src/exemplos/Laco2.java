package exemplos;

public class Laco2 {

    public static void main(String[] args) {
        int a = 0, b = 15;

        while (a <= b) {
            a++;
            System.out.println("O valor de A dentro do while é: " + a);
        }
    }
}
