package aplicacao;

import entidades.Veiculo;

public class Teste {

	public static void main(String[] args) {
		Veiculo veiculo1 = new Veiculo();
		veiculo1.setPlaca("AGX-5220");
		veiculo1.setMarca("Fiat");
		veiculo1.setModelo("147");
		veiculo1.setCor("Bege");
		veiculo1.setVelocMax(160);
		veiculo1.setQtdRodas(4);
		veiculo1.getMotor().setPotencia(58);
		veiculo1.getMotor().setQtdPist(4);

		Veiculo veiculo2 = new Veiculo();
		veiculo2.setPlaca("AFT-4720");
		veiculo2.setMarca("Ford");
		veiculo2.setModelo("Pampa");
		veiculo2.setCor("Preta");
		veiculo2.setVelocMax(170);
		veiculo2.setQtdRodas(4);
		veiculo2.getMotor().setPotencia(80);
		veiculo2.getMotor().setQtdPist(4);

		Veiculo veiculo3 = new Veiculo();
		veiculo3.setPlaca("AGH-5793");
		veiculo3.setMarca("Chevrolet");
		veiculo3.setModelo("Chevette");
		veiculo3.setCor("Prata");
		veiculo3.setVelocMax(180);
		veiculo3.setQtdRodas(4);
		veiculo3.getMotor().setPotencia(75);
		veiculo3.getMotor().setQtdPist(4);

		Veiculo veiculo4 = new Veiculo();
		veiculo4.setPlaca("MHT-3275");
		veiculo4.setMarca("Volkswagen");
		veiculo4.setModelo("Fusca");
		veiculo4.setCor("Amarela");
		veiculo4.setVelocMax(120);
		veiculo4.setQtdRodas(4);
		veiculo4.getMotor().setPotencia(52);
		veiculo4.getMotor().setQtdPist(4);

		Veiculo veiculo5 = new Veiculo();
		veiculo5.setPlaca("BHT-3207");
		veiculo5.setMarca("Volkswagen");
		veiculo5.setModelo("Kombi");
		veiculo5.setCor("Azul");
		veiculo5.setVelocMax(110);
		veiculo5.setQtdRodas(4);
		veiculo5.getMotor().setPotencia(56);
		veiculo5.getMotor().setQtdPist(4);

		System.out.println(veiculo1);
		System.out.println(veiculo2);
		System.out.println(veiculo3);
		System.out.println(veiculo4);
		System.out.println(veiculo5);
	}

}
